import os
from flask import Flask
from flask_healthz import healthz
from flask_healthz import HealthError
from prometheus_flask_exporter import PrometheusMetrics


app = Flask(__name__)
metrics = PrometheusMetrics(app)
app.register_blueprint(healthz, url_prefix="/healthz")


def printok():
    return 'Ok'

def liveness():
    try:
        printok()
    except Exception:
        raise HealthError("Can't connect to the file")

def readiness():
    try:
        printok()
    except Exception:
        raise HealthError("Can't connect to the file")

app.config.update(
    HEALTHZ = {
        "live": "app.liveness",
        "ready": "app.readiness",
        "err": "app.err"
    }
)

@app.route("/")
def index():
    return "<p>Hello, World!</p>"

@app.route("/hostname")
def hostname():
    return os.uname()[1]


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)